from django.db import models
from django.contrib.auth.models import User
import datetime

class StatusModel(models.Model):
    status = models.CharField(max_length=300)
    date_added = models.DateTimeField(auto_now_add=True,null=True)
    
    

    def __unicode__(self):
        return self.status
