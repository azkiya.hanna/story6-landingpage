from django.shortcuts import render  
from .models import StatusModel
from .forms import StatusForm

def StatusFunction(request):
    dictio={}
    data = StatusModel.objects.all()
    form = StatusForm()

    if request.method == "POST":
        form = StatusForm(request.POST or None)
        dictio['form']= StatusForm()
        if form.is_valid():
            model = StatusModel(status=form['status'].value())
            model.save()
            dictio['data'] = data
            return render(request,'landingpage.html',dictio)
    return render(request,'landingpage.html',{'form':form})
    

# Create your views here.
