from django.test import TestCase,Client, LiveServerTestCase
from django.urls import reverse,resolve
from .views import StatusFunction
from .models import StatusModel
from .forms import StatusForm
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class LandingPageTest(TestCase):
    def test_apakah_ada_landing_page(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code,200)
    def test_apakah_file_html_landingpage_digunakan(self):
        c = Client()
        response = c.get('')
        self.assertTemplateUsed(response, 'landingpage.html')
    def test_apakah_fungsi_status_dijalankan(self):
        response = resolve(reverse('status'))
        self.assertEqual(response.func,StatusFunction)
    def buat_model_Status_Model(self, status = "Status trial"):
        return StatusModel.objects.create(status=status)
    def test_apakah_ada_model_Status_Model(self):
        k = self.buat_model_Status_Model()
        self.assertTrue(isinstance(k,StatusModel))
        self.assertEqual(k.__unicode__(),k.status)
    def test_apakah_form_dapat_dibuat_dan_tersimpan_di_model(self):
        data = { 'status' : 'Coba Status melalui form'}
        form_status = StatusForm(data=data)
        self.assertTrue(form_status.is_valid())
        form_status.save()
        semua_isinya = StatusModel.objects.all()
        self.assertEqual(semua_isinya.count(),1)
    def test_apakah_masukkan_invalid_tidak_diterima(self):
        data = { 'status' : ''}
        form_status = StatusForm(data=data)
        self.assertFalse(form_status.is_valid())
    def test_halaman_awal_ketika_landing_page_dibuka(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('<form',content)
        self.assertIn('What is your Status?',content)
    def test_apakah_masukan_dengan_method_post_disimpan_dan_dimunculkan_kembali(self):
        c = Client()
        data = {'status' : 'Coba method status post'}
        response = c.post('',data)
        content = response.content.decode('utf8')
        self.assertIn('Coba method status post',content)
        self.assertEqual(StatusModel.objects.all().count(), 1)
        self.assertEqual(
            StatusModel.objects.get(id=1).status, 'Coba method status post'
        )
    def test_apakah_fungsi_StatusFunction_bekerja_saat_mendapat_request_GET(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed('landingpage.html')
        self.assertIn("<form",content)
        self.assertIn('<input',content)
class FunctionalLandingPageTest(LiveServerTestCase):
    def setUp(self):
        chrome_op = Options()
        chrome_op.add_argument('--no-sandbox'),
        chrome_op.add_argument('--headless'),
        chrome_op.add_argument('disable-dev-shm-usage'),
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_op)
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()


    def test_apakah_title_sesuai(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        self.assertIn("Status",selenium.title)
        time.sleep(5)

    def test_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        new_status = selenium.find_element_by_name("status")
        new_status.send_keys("Coba Coba")
        submit = selenium.find_element_by_name('add')
        submit.click()
        self.assertIn('Coba Coba',selenium.page_source)
